/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamen1;

/**
 *
 * @author Black
 */
public class Recibo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        MetodosRecibo recibo = new MetodosRecibo();
       recibo.setNumContrato(102); 
       recibo.setFechaPago("21 Marzo 2019");
       recibo.setNombreEmpleado("Jose Lopez");
       recibo.setDomicilio("Av del Sol 1200");
       recibo.setTipoContrato(1);
       recibo.setNivelEstudios(1);
       recibo.setPagoBase(700.0f);
       recibo.setDiasTrabajados(15);   
     
       //OBJETO CONSTRUIDO POR ARGUMENTOS
       System.out.println ("Numero de Contrato:  " + recibo.getNumContrato());
       System.out.println("Fecha de Pago:       " + recibo.getFechaPago());
       System.out.println("Nombre Empleado:     " + recibo.getNombreEmpleado());
       System.out.println("Domicilio:           " + recibo.getDomicilio());
       System.out.println("Tipo de Contrato:    " + recibo.getTipoContrato());
       System.out.println("Nivel de Estudios:   " + recibo.getNivelEstudios());
       System.out.println("Pago Diario Base:    " + recibo.getPagoBase());
       System.out.println("Dias Trabajados:     " + recibo.getDiasTrabajados());
       //GENERA OBJETO CONSTRUIDO POR COPIA
       System.out.println("     El Subtotal es: " + recibo.calculoSubtotal());
       System.out.println("     El Impuesto es: " + recibo.calculoImpuesto());
       System.out.println("     El Total a Pagar es: " + recibo.calculoTotal());
        
          System.out.println("    E J E M P L O  2    ");
         
       recibo.setNumContrato(103); 
       recibo.setFechaPago("23 Marzo 2019");
       recibo.setNombreEmpleado("Maria Acosta ");
       recibo.setDomicilio("Av del Sol 1200");
       recibo.setTipoContrato(2);
       recibo.setNivelEstudios(2);
       recibo.setPagoBase(700.0f);
       recibo.setDiasTrabajados(15);   
       
       //OBJETO CONSTRUIDO POR ARGUMENTOS
       System.out.println ("Numero de Contrato:  " + recibo.getNumContrato());
       System.out.println("Fecha de Pago:       " + recibo.getFechaPago());
       System.out.println("Nombre Empleado:     " + recibo.getNombreEmpleado());
       System.out.println("Domicilio:           " + recibo.getDomicilio());
       System.out.println("Tipo de Contrato:    " + recibo.getTipoContrato());
       System.out.println("Nivel de Estudios:   " + recibo.getNivelEstudios());
       System.out.println("Pago Diario Base:    " + recibo.getPagoBase());
       System.out.println("Dias Trabajados:     " + recibo.getDiasTrabajados());
       
       //GENERA OBJETO CONSTRUIDO POR COPIA
       System.out.println("     El SubTotal es: " + recibo.calculoSubtotal());
       System.out.println("     El Impuesto es: " + recibo.calculoImpuesto());
       System.out.println("     El Total a Pagar es: " + recibo.calculoTotal());
    }
}